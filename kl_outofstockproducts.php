<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Kl_outofstockproducts extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'kl_outofstockproducts';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Korel';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Change the category for products without stock');
        $this->description = $this->l('Change the category for products without stock Change the category for products without stock Change the category for products without stockChange the category for products without stock');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('KL_OUTOFSTOCKPRODUCTS_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('KL_OUTOFSTOCKPRODUCTS_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitKl_outofstockproductsModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitKl_outofstockproductsModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-arrow-down "></i>',
                        'desc' => $this->l('Minimum Product Id'),
                        'name' => 'KL_OUTOFSTOCKPRODUCTS_MIN_PROD_ID',
                        'required' => 'required',
                        'label' => $this->l('From Product ID:'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-arrow-up"></i>',
                        'desc' => $this->l('Maximum Product Id'),
                        'name' => 'KL_OUTOFSTOCKPRODUCTS_MAX_PROD_ID',
                        'required' => 'required',
                        'label' => $this->l('To Product ID:'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-arrow-up"></i>',
                        'desc' => $this->l('Move to Category ID:'),
                        'name' => 'KL_OUTOFSTOCKPRODUCTS_CAT_ID',
                        'required' => 'required',
                        'label' => $this->l('Category ID:'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Run'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'KL_OUTOFSTOCKPRODUCTS_MIN_PROD_ID' => Configuration::get('KL_OUTOFSTOCKPRODUCTS_MIN_PROD_ID'),
            'KL_OUTOFSTOCKPRODUCTS_MAX_PROD_ID' => Configuration::get('KL_OUTOFSTOCKPRODUCTS_MAX_PROD_ID'),
            'KL_OUTOFSTOCKPRODUCTS_CAT_ID' => Configuration::get('KL_OUTOFSTOCKPRODUCTS_CAT_ID')
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }

        $form_values = $this->getConfigFormValues();
        if($this->moveProductsToCategory($form_values['KL_OUTOFSTOCKPRODUCTS_CAT_ID'], $form_values['KL_OUTOFSTOCKPRODUCTS_MIN_PROD_ID'],$form_values['KL_OUTOFSTOCKPRODUCTS_MAX_PROD_ID'])) {
            $this->adminDisplayWarning('Products with IDs from: ' . $form_values['KL_OUTOFSTOCKPRODUCTS_MIN_PROD_ID'] . ' to: ' . $form_values['KL_OUTOFSTOCKPRODUCTS_MAX_PROD_ID'] . ', was succesfully moved to the category with ID: ' . $form_values['KL_OUTOFSTOCKPRODUCTS_CAT_ID'] . ' ');
            $this->adminDisplayWarning('Last moved product ID:'.Configuration::get('last_moved_product_id_outofstock'));
            foreach (array_keys($form_values) as $key) {
                Configuration::updateValue($key, '0');
            }
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    /**
     * @param null $min_prod_id
     * @param null $max_prod_id
     * @param $category_id
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public static function moveProductsToCategory($category_id, $min_prod_id = null, $max_prod_id = null )
    {
        if(is_null($min_prod_id)){
            $min_prod_id = 0;
        }
        if(is_null($max_prod_id)){
            $max_prod_id = 1000000;
        }
        $sqlGetOutOfStockProductIds = 'SELECT DISTINCT(`id_product`) FROM `ps_stock_available` WHERE `id_product` BETWEEN '.(int)$min_prod_id.'  AND '.(int)$max_prod_id.' AND `id_product_attribute` = 0 AND `quantity` < 1 ORDER BY `ps_stock_available`.`id_product` ASC';
        $oufOfStockProductIds = Db::getInstance()->ExecuteS($sqlGetOutOfStockProductIds);

        foreach ($oufOfStockProductIds as $key => $products) {
            try {
                $productId = $products['id_product'];
                $t = new Product((int)$productId);
                $t->deleteCategories();
                $t->addToCategories((int)$category_id);
                Configuration::updateValue('last_moved_product_id_outofstock', $productId);
            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
            }
        }

        return true;
    }
}
